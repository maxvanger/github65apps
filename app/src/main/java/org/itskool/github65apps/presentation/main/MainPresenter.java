package org.itskool.github65apps.presentation.main;

import android.support.annotation.NonNull;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import org.itskool.github65apps.App;
import org.itskool.github65apps.domain.interactor.KeywordsInteractor;
import org.itskool.github65apps.domain.model.SearchText;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vanger on 29.11.2017.
 */
@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    @NonNull
    private final KeywordsInteractor keywords;
    private Disposable keywordsDisposable;

    @Inject
    public MainPresenter(KeywordsInteractor keywords) {
        this.keywords = keywords;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        keywordsDisposable = keywords.load("")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(Observable::fromIterable)
                .map(SearchText::query)
                .toList()
                .subscribe(list -> getViewState().takeSuggestions(list),
                        e -> Log.d(App.TAG, "keywords error: " + e.getMessage()));
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        view.setTextInput(keywords.lastKeyword());
        searchingRequest(keywords.lastKeyword());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (keywordsDisposable != null) keywordsDisposable.dispose();
    }

    public void searchingRequest(String request){
        App.APP.router().navigateTo(MainActivity.FRAG_LIST, request);
    }
}
