package org.itskool.github65apps.presentation.detail;

import org.itskool.github65apps.domain.interactor.FavoritesInteractor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Vanger on 05.01.2018.
 */
@Module
public class DetailModule {

    @Provides
    @DetailScope
    DetailPresenter providePresenter(FavoritesInteractor interactor){
        return new DetailPresenter(interactor);
    }
}
