package org.itskool.github65apps.presentation.detail;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Vanger on 05.01.2018.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface DetailScope {
}
