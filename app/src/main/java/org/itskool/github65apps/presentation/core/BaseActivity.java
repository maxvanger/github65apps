package org.itskool.github65apps.presentation.core;

import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;

import org.itskool.github65apps.R;

/**
 * Created by Vanger on 13.06.2017.
 */

public class BaseActivity extends MvpAppCompatActivity {
    private ProgressDialog mProgressDialog;

    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.progress_dialog);
            mProgressDialog.setCancelable(true);
            mProgressDialog.getWindow()
                    .setBackgroundDrawable(
                            new ColorDrawable(
                                    getResources().getColor(R.color.transparent_color)));
        }

        mProgressDialog.show();
        mProgressDialog.setContentView(R.layout.progress_layout);
    }

    public void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
    }

    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
