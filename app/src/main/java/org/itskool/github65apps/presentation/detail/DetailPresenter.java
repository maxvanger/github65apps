package org.itskool.github65apps.presentation.detail;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import org.itskool.github65apps.App;
import org.itskool.github65apps.domain.interactor.FavoritesInteractor;

import javax.inject.Inject;

/**
 * Created by Vanger on 05.01.2018.
 */
@InjectViewState
public class DetailPresenter extends MvpPresenter<DetailView> {

    private FavoritesInteractor interactor;

    @Inject
    public DetailPresenter(FavoritesInteractor interactor) {
        this.interactor = interactor;
    }

    void clickLike(int gitId, boolean checked) {
        Log.d(App.TAG,"DetailPresenter::gitId="+gitId+"/"+checked);
/*        if (checked){
            interactor.addFavorite(gitId);
        } else {
            interactor.removeFavorite(gitId);
        }*/
        interactor.changeFavorite(gitId);
    }

    void repoId(int i) {
        Log.d(App.TAG,"DetailPresenter::repoId"+i);
        getViewState().hasLike(interactor.getFavoriteIds().contains(i));
    }

}
