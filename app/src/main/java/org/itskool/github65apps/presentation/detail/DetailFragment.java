package org.itskool.github65apps.presentation.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.squareup.picasso.Picasso;

import org.itskool.github65apps.App;
import org.itskool.github65apps.R;
import org.itskool.github65apps.domain.model.Repo;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.Subcomponent;

/**
 * Created by Vanger on 19.11.2017.
 */

public class DetailFragment extends MvpAppCompatFragment implements DetailView {
    private static final String ARG_REPO = "ARG_REPO";

    @BindView(R.id.detail_ava)
    ImageView detailAva;
    @BindView(R.id.detail_author)
    TextView detailAuthor;
    @BindView(R.id.detail_repo_name)
    TextView detailRepoName;
    @BindView(R.id.detail_repo_description)
    TextView detailRepoDescription;

    @BindView(R.id.watchers_count)
    TextView watchersCount;
    @BindView(R.id.forks_count)
    TextView forksCount;
    @BindView(R.id.stars_count)
    TextView starsCount;
    @BindView(R.id.created_at_tv)
    TextView createdAt;
    @BindView(R.id.updated_at_tv)
    TextView updatedAt;
    @BindView(R.id.like)
    CheckBox checkLike;

    Unbinder butter;
    @Inject
    @InjectPresenter
    DetailPresenter presenter;
    private Repo repo;

    public static DetailFragment newInstance(Repo repo) {
        Bundle args = new Bundle();
        DetailFragment fragment = new DetailFragment();
        args.putParcelable(ARG_REPO, repo);
        fragment.setArguments(args);
        return fragment;
    }

    @ProvidePresenter
    DetailPresenter getPresenter() {
        App.APP.appComponent().detailComponent(new DetailModule()).inject(this);
        return presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.detail_fragment, container, false);
        butter = ButterKnife.bind(this, view);

        repo = getArguments().getParcelable(ARG_REPO);
        fillView(repo);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (butter != null) {
            butter.unbind();
        }
    }

    private void fillView(Repo r) {

        presenter.repoId(r.gitId());

        detailAuthor.setText(r.userName());
        detailRepoDescription.setText(r.description());
        watchersCount.setText(String.valueOf(r.watchers()));
        forksCount.setText(String.valueOf(r.forks()));
        starsCount.setText(String.valueOf(r.stars()));

        String s = r.title();
        detailRepoName.setText(s.substring(s.indexOf("/") + 1));

        s = r.updateAt();
        if (s != null && s.length() > 0)
            updatedAt.setText(s.substring(0, s.indexOf("T")));

        s = r.createdAt();
        if (s != null && s.length() > 0)
            createdAt.setText(s.substring(0, s.indexOf("T")));

        Picasso.with(getActivity())
                .load(r.userAva())
                .placeholder(R.drawable.ic_github)
                .into(detailAva);
    }

    @OnClick(R.id.open_in_browse)
    void openInBrowse() {
        Intent browse = new Intent(Intent.ACTION_VIEW);
        browse.setData(Uri.parse(repo.htmlUrl()));
        startActivity(browse);
    }

    @OnClick(R.id.share)
    void share() {
        Intent sharing = new Intent(Intent.ACTION_SEND);
        sharing.setType("text/plain");
        sharing.putExtra(android.content.Intent.EXTRA_TEXT, repo.htmlUrl());
        startActivity(sharing);
    }

    @OnClick(R.id.like)
    void like() {
        presenter.clickLike(repo.gitId(), checkLike.isChecked());
    }

    @Override
    public void hasLike(boolean isLiked) {
        checkLike.setChecked(isLiked);
    }

    @Subcomponent(modules = DetailModule.class)
    @DetailScope
    public interface DetailComponent {
        void inject(DetailFragment fragment);
    }
}
