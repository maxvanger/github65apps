package org.itskool.github65apps.presentation.main;

import org.itskool.github65apps.domain.interactor.KeywordsInteractor;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Vanger on 17.01.2018.
 */
@Module
public class MainActivityModule {
    @Provides
    MainPresenter providePresenter(KeywordsInteractor keywords){
        return new MainPresenter(keywords);
    }
}
