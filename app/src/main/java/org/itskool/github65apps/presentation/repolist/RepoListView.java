package org.itskool.github65apps.presentation.repolist;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import org.itskool.github65apps.domain.model.Repo;
import org.itskool.github65apps.domain.model.SearchText;

import java.util.List;

/**
 * Created by Vanger on 03.11.2017.
 */

public interface RepoListView extends MvpView {
    @StateStrategyType(AddToEndSingleStrategy.class)
    void takeList(List<Repo> list);

    @StateStrategyType(SkipStrategy.class)
    void updateList(int prevSize);

    @StateStrategyType(SkipStrategy.class)
    void notifyAdapter();

    @StateStrategyType(SkipStrategy.class)
    void listOver(boolean isOver);
}
