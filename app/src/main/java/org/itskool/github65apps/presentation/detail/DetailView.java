package org.itskool.github65apps.presentation.detail;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by Vanger on 05.01.2018.
 */

public interface DetailView extends MvpView{
    @StateStrategyType(AddToEndSingleStrategy.class)
    void hasLike(boolean isLiked);
}
