package org.itskool.github65apps.presentation.repolist;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.itskool.github65apps.App;
import org.itskool.github65apps.R;
import org.itskool.github65apps.domain.model.Repo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static org.itskool.github65apps.App.PAGE_SIZE;

/**
 * Created by Vanger on 12.11.2017.
 */

public class RepoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOT = 1;

    private static final int DETAILS_LENGTH = 350;
    private List<Repo> items = new ArrayList<>();
    private boolean isListEnd;
    private boolean isLastItems;
    private boolean isFirstItems;
    private RecyclerClickListener clickListener;

    public void take(List<Repo> list) {
        items = list;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_FOOT) {
            View v = inflater.inflate(R.layout.progress_layout, parent, false);
            return new FootHolder(v);
        } else {
            View v = inflater.inflate(R.layout.repo_item, parent, false);
            return new RepoHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RepoHolder) {
            ((RepoHolder) holder).show(items.get(position));
        }
        isLastItems = getItemCount() - position < PAGE_SIZE / 2;
        isFirstItems = position < PAGE_SIZE / 2 && items.size() > 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position == items.size() ? TYPE_FOOT : TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        if (!isListEnd)
            return items.size() + 1;          // +1 for footer
        else {
            return items.size();            // drop footer - cause of end of the list
        }
    }

    Boolean isFirstItems() {
        return isFirstItems;
    }

    Boolean isLastItems() {
        return isLastItems;
    }

    void clickListener(RecyclerClickListener l) {
        clickListener = l;
    }

    void listOver(boolean isOver) {
        isListEnd = isOver;
        if (isListEnd) {
            notifyItemChanged(items.size());        //drop footer item
        }
    }

    public interface RecyclerClickListener {
        void itemClick(int position);
    }

    public class RepoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.owner_image)
        ImageView ownerImage;
        @BindView(R.id.repo_name_txt)
        TextView repoNameTxt;
        @BindView(R.id.repo_detail_txt)
        TextView repoDetailTxt;
        @BindView(R.id.repo_stars_txt)
        TextView repoStartsTxt;
        @BindView(R.id.position)
        TextView position;

        public RepoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void show(Repo repo) {
            String descr = "";
            if (repo.description() != null) {
                descr = repo.description().length() < DETAILS_LENGTH ?
                        repo.description() :
                        repo.description().substring(0, DETAILS_LENGTH);
            }
            repoDetailTxt.setText(descr);
            repoNameTxt.setText(repo.title());
            repoStartsTxt.setText(String.valueOf(repo.stars()));
            position.setText(String.valueOf(getAdapterPosition()));
            Picasso.with(((ViewGroup) ownerImage.getParent()).getContext())
                    .load(repo.userAva())
                    .placeholder(R.drawable.ic_github)
                    .into(ownerImage);
        }

        @Override
        public void onClick(View view) {
            clickListener.itemClick(getAdapterPosition());
        }
    }

    public class FootHolder extends RecyclerView.ViewHolder {
        FootHolder(View itemView) {
            super(itemView);
        }
    }

}
