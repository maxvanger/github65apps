package org.itskool.github65apps.presentation.repolist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import org.itskool.github65apps.App;
import org.itskool.github65apps.R;
import org.itskool.github65apps.di.RepoListModule;
import org.itskool.github65apps.domain.model.Repo;
import org.itskool.github65apps.domain.model.SearchText;
import org.itskool.github65apps.presentation.detail.DetailFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Vanger on 19.11.2017.
 */

public class PagerFragment extends MvpAppCompatFragment implements RepoListView {

    private static final String ARG_POSITION = "ARG_POSITION";
    @Inject
    @InjectPresenter
    RepoListPresenter presenter;
    private ViewPager pager;
    private RepoPagerAdapter adapter;
    private int currentPosition;
    private boolean isNewRequest;

    public static PagerFragment newInstance(int position) {

        Bundle args = new Bundle();

        PagerFragment fragment = new PagerFragment();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @ProvidePresenter
    RepoListPresenter getPresenter() {
//        App.repoListComponent().inject(this);
        return App.APP.appComponent().repoListComponent(new RepoListModule()).getListPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pager_fragment, container, false);
        currentPosition = getArguments().getInt(ARG_POSITION, 0);

        adapter = new RepoPagerAdapter(getChildFragmentManager());
        pager = view.findViewById(R.id.pager_repos);
        pager.setAdapter(adapter);

        if (currentPosition > 0 && currentPosition < adapter.getCount()) {
            pager.setCurrentItem(currentPosition);
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        currentPosition = pager.getCurrentItem();
        outState.putInt(ARG_POSITION, currentPosition);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getInt(ARG_POSITION, 0);
        }
        super.onViewStateRestored(savedInstanceState);
    }

    //region ================= RepoListView =================

    @Override
    public void takeList(List<Repo> list) {
        int position = isNewRequest ? 0 : currentPosition;
        isNewRequest = false;
        adapter.take(list);
        pager.setCurrentItem(position);
    }

    @Override
    public void updateList(int prevSize) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void notifyAdapter() {
    }

    @Override
    public void listOver(boolean isOver) {
    }

    //endregion

    static class RepoPagerAdapter extends FragmentStatePagerAdapter {

        List<Repo> list = new ArrayList<>();

        public RepoPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return DetailFragment.newInstance(list.get(position));
        }

        @Override
        public int getCount() {
            return list.size();
        }

        public void take(List<Repo> repos) {
            list = repos;
            notifyDataSetChanged();
        }

/*
        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }
*/

    }

}
