package org.itskool.github65apps.presentation.main;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import org.itskool.github65apps.domain.model.SearchText;

import java.util.List;


/**
 * Created by Vanger on 02.12.2017.
 */

public interface MainView extends MvpView {

    void showMessage(String text);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setSearchBarVisible(boolean isVisible);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void setTextInput(String text);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void takeSuggestions(List<String> list);
}
