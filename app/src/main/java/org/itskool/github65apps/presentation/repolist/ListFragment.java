package org.itskool.github65apps.presentation.repolist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import org.itskool.github65apps.App;
import org.itskool.github65apps.R;
import org.itskool.github65apps.di.RepoListModule;
import org.itskool.github65apps.domain.model.Repo;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by Vanger on 05.11.2017.
 */

public class ListFragment extends MvpAppCompatFragment implements RepoListView, RepoAdapter.RecyclerClickListener {

    private static final String SEARCHING_REQUEST = "SEARCHING_REQUEST";

    @Inject
    @InjectPresenter()
    RepoListPresenter presenter;

    String query;
    @BindView(R.id.repo_recycler)
    RecyclerView repoRecycler;

    Unbinder bk;
    private RepoAdapter adapter;

    private BehaviorSubject<Boolean> scrollUp = BehaviorSubject.create();
    private BehaviorSubject<Boolean> scrollDown = BehaviorSubject.create();

    public static ListFragment newInstance(String query) {
        Bundle args = new Bundle();
        args.putString(SEARCHING_REQUEST, query);
        ListFragment fragment = new ListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @ProvidePresenter()
    RepoListPresenter getPresenter() {
//        App.repoListComponent().inject(this);
        return App.APP.appComponent().repoListComponent(new RepoListModule()).getListPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(App.TAG, "ListFragment onCreate: " + this);
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            query = getArguments().getString(SEARCHING_REQUEST, "");
            presenter.searchText(query);
        }

        adapter = new RepoAdapter();
        adapter.clickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(App.TAG, "ListFragment onCreateView: " + this);

        View view = inflater.inflate(R.layout.list_fragment, container, false);
        bk = ButterKnife.bind(this, view);

        initRecycler();

        return view;
    }

    private void initRecycler() {
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.supportsPredictiveItemAnimations();
        repoRecycler.setLayoutManager(llm);
        repoRecycler.setAdapter(adapter);
        repoRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    scrollUp.onNext(adapter.isLastItems());
                } else {
                    scrollDown.onNext(adapter.isFirstItems());
                }
            }
        });
        presenter.prevPageRequest(scrollDown);
        presenter.nextPageRequest(scrollUp);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bk.unbind();
    }

    @Override
    public void itemClick(int position) {
        presenter.itemClick(position);
    }

    //region ================= ItemListView =================
    @Override
    public void takeList(List<Repo> list) {
        if (adapter != null) {
            adapter.take(list);
        }
    }

    @Override
    public void updateList(int prevSize) {
//        handler.post(() -> adapter.notifyItemInserted(prevSize));
        adapter.notifyDataSetChanged();
    }

    @Override
    public void listOver(boolean isOver) {
        Log.d(App.TAG, "ListFragment.listOver:" + isOver);
        adapter.listOver(isOver);
    }

    @Override
    public void notifyAdapter() {
        adapter.notifyDataSetChanged();
    }

    //endregion
}
