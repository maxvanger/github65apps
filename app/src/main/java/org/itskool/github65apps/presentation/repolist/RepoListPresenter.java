package org.itskool.github65apps.presentation.repolist;

import android.support.annotation.NonNull;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import org.itskool.github65apps.App;
import org.itskool.github65apps.domain.interactor.KeywordsInteractor;
import org.itskool.github65apps.domain.interactor.RepoListInteractor;
import org.itskool.github65apps.domain.model.Repo;
import org.itskool.github65apps.presentation.main.MainActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.requery.reactivex.ReactiveResult;

import static org.itskool.github65apps.App.PAGE_SIZE;

/**
 * Created by Vanger on 03.11.2017.
 */
@InjectViewState()
public class RepoListPresenter extends MvpPresenter<RepoListView> {

    @NonNull
    private final RepoListInteractor git;
    @NonNull
    private final KeywordsInteractor keywords;

    private Disposable disposable;
    private List<Repo> repoList;

    @Inject
    public RepoListPresenter(RepoListInteractor git, KeywordsInteractor keywords) {
        this.git = git;
        this.keywords = keywords;
        repoList = new ArrayList<>(PAGE_SIZE * 2);
    }

    @Override
    public void attachView(RepoListView view) {
        super.attachView(view);
        view.takeList(repoList);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (disposable != null) {
            disposable.dispose();
        }
    }


    public void searchText(String keyword) {
        keywords.lastKeyword(keyword);
        repoList.clear();
        getViewState().notifyAdapter();

        loadData(keyword);
    }

    private void loadData(String search) {
        Log.d(App.TAG, "RepoListPresenter.loadData() " + search);

        disposable = git.loadData(search)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onLoadSuccess, this::onLoadFail, this::onLoadComplete);
    }

    public void nextPageRequest(Observable<Boolean> position) {

        position.distinctUntilChanged()
                .filter(b -> b)
                .subscribe(b -> {
                    Log.d(App.TAG, "nextPageRequest: " + b);
                    git.nextPage();
                });
    }

    private void onLoadSuccess(List<Repo> list) {
        Log.d(App.TAG, "RepoListPresenter onLoadSuccess: " + list.size());

        getViewState().listOver(list.size() < PAGE_SIZE);
        int prevSize = repoList.size() + 1;     //+1 because of FootItem in RecyclerView
        repoList.addAll(list);

        getViewState().updateList(prevSize);
    }

    private void onLoadFail(Throwable e) {
        Log.d(App.TAG, e.getMessage());
        getViewState().listOver(true);
    }

    private void onLoadComplete() {
        disposable.dispose();
        getViewState().listOver(true);
    }

    void prevPageRequest(Observable<Boolean> position) {
/*        position.distinctUntilChanged()
                .filter(b -> b)
                .subscribe(b -> {
                    Log.d(App.TAG, "presenter: PREV page request");
                    git.prevPage()
                            .subscribe(list -> repos
                                    .repeat(1)
                                    .subscribe(
                                            this::onLoadSuccess,
                                            this::onLoadFail));
                });*/
    }

    public ReactiveResult<Repo> getResults(String query) {
        return git.getResults(query);
    }

    void itemClick(int position) {
        App.APP.router().navigateTo(MainActivity.FRAG_DETAIL, repoList.get(position));
    }

    void onBackClick(){
        App.APP.router().exit();
    }
}
