package org.itskool.github65apps.presentation.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import org.itskool.github65apps.App;
import org.itskool.github65apps.R;
import org.itskool.github65apps.domain.model.Repo;
import org.itskool.github65apps.presentation.core.BaseActivity;
import org.itskool.github65apps.presentation.detail.DetailFragment;
import org.itskool.github65apps.presentation.repolist.ListFragment;
import org.itskool.github65apps.presentation.repolist.PagerFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.Subcomponent;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.android.SupportAppNavigator;

/**
 * Created by Vanger on 02.12.2017.
 */

public class MainActivity extends BaseActivity implements MainView {

    public static final String FRAG_LIST = "FRAG_LIST";
    public static final String FRAG_PAGER = "FRAG_PAGER";
    public static final String FRAG_DETAIL = "FRAG_DETAIL";

    @BindView(R.id.search_tv)
    AutoCompleteTextView searchTextView;

    @BindView(R.id.searching_bar)
    ViewGroup searchingBar;

    @Inject
    @InjectPresenter
    MainPresenter presenter;

    private FragmentManager fm;
    private InputMethodManager imm;
    private ArrayAdapter<String> suggestions;
    private int clickPosition;
    private String currentFrag = FRAG_LIST;

    private Navigator navigator = new SupportAppNavigator(this, R.id.main_frame) {
        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case FRAG_DETAIL:
                    return DetailFragment.newInstance((Repo) data);
                case FRAG_LIST:
                    return ListFragment.newInstance((String) data);
                case FRAG_PAGER:
                    return PagerFragment.newInstance(clickPosition);
                default:
                    throw new IllegalArgumentException();
            }
        }

        @Override
        protected Intent createActivityIntent(String screenKey, Object data) {
            return null;
        }
    };

    @ProvidePresenter
    MainPresenter providePresenter() {
        return App.APP
                .appComponent()
                .mainActivity(new MainActivityModule())
                .providePresenter();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        fm = getSupportFragmentManager();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        searchTextView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                startSearch();
                return true;
            }
            return false;
        });
        suggestions = new ArrayAdapter<>(this, R.layout.suggestion_item);
        searchTextView.setAdapter(suggestions);

        currentFrag = FRAG_LIST;
    }

    @Override
    protected void onResume() {
        super.onResume();
        searchTextView.clearFocus();
        App.APP.navigatorHolder().setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.APP.navigatorHolder().removeNavigator();
    }

    @Override
    protected void onStop() {
        super.onStop();
        imm = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick(R.id.search_btn)
    void startSearch() {
        imm.hideSoftInputFromWindow(searchTextView.getRootView().getWindowToken(), 0);
        String query = searchTextView.getText().toString();
        presenter.searchingRequest(query);
    }

    @Override
    public void setTextInput(String text) {
        searchTextView.setText(text);
    }

    @Override
    public void takeSuggestions(List<String> suggests) {
        suggestions.clear();
        suggestions.addAll(suggests);
        suggestions.notifyDataSetChanged();
    }

    @Override
    public void setSearchBarVisible(boolean isVisible) {
        searchingBar.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    @Subcomponent(modules = MainActivityModule.class)
    public interface Component {
        MainPresenter providePresenter();
    }
}
