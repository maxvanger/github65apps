package org.itskool.github65apps.data.core;

import org.itskool.github65apps.domain.model.Repo;
import org.itskool.github65apps.domain.model.SearchText;

import java.util.List;

import io.reactivex.Observable;
import io.requery.reactivex.ReactiveResult;

/**
 * Created by Vanger on 03.12.2017.
 */

public interface LocalSource extends DataSource<Repo> {

    ReactiveResult<Repo> requeryResult(String q);
}
