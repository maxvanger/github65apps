package org.itskool.github65apps.data.db;

import android.support.annotation.Nullable;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.requery.Converter;

/**
 * Created by Vanger on 27.11.2017.
 */

public class ListLongConverter implements Converter<List<Long>, String> {
    private static final String SEPARATOR = "_";

    @Override
    public Class<List<Long>> getMappedType() {
        return (Class) List.class;
    }

    @Override
    public Class<String> getPersistedType() {
        return String.class;
    }

    @Nullable
    @Override
    public Integer getPersistedSize() {
        return null;
    }

    @Override
    public String convertToPersisted(List<Long> list) {
        if (list == null) return "";

        StringBuilder sb = new StringBuilder();
        int size = list.size();
        int index = 0;
        for (Long id : list) {
            ++index;
            sb.append(String.valueOf(id));
            if (index < size) {
                sb.append(SEPARATOR);
            }
        }
        return sb.toString();
    }

    @Override
    public List<Long> convertToMapped(Class<? extends List<Long>> type, @Nullable String value) {
        if (value == null) {
            return Collections.emptyList();
        }
        List<Long> list = Observable.fromArray(value.split(SEPARATOR))
                .map(Long::valueOf)
                .toList().blockingGet();
        return list;
    }
}
