package org.itskool.github65apps.data.core;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by Vanger on 03.12.2017.
 */

public interface DataSource<T> {

    Single<List<T>> loadPage(String query, int page);

    void save(T t, String tag);

    void saveAll(List<T> list, String tag);

    int getItemsCount();
}
