package org.itskool.github65apps.data.net;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vanger on 12.11.2017.
 */

public class RepoDTO {
    @SerializedName("full_name")
    private String fullName;

    @SerializedName("description")
    private String description;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updateAt;

    @SerializedName("stargazers_count")
    private int starsCount;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("owner")
    private UserDTO owner;

    @SerializedName("id")
    private int gitId;

    @SerializedName("forks_count")
    private int forksCount;

    @SerializedName("watchers_count")
    private int watchersCount;

    public String fullName() {
        return fullName;
    }

    public String htmlUrl() {
        return htmlUrl;
    }

    public String description() {
        return description;
    }

    public String createdAt() {
        return createdAt;
    }

    public String updateAt() {
        return updateAt;
    }

    public int starsCount() {
        return starsCount;
    }

    public UserDTO owner() {
        return owner;
    }

    public int forksCount() { return forksCount; }

    public int watchersCount() { return watchersCount; }

    public int gitId() {
        return gitId;
    }
}
