package org.itskool.github65apps.data.core;

import android.content.SharedPreferences;
import android.util.Log;

import org.itskool.github65apps.App;
import org.itskool.github65apps.domain.model.FavRepo;
import org.itskool.github65apps.domain.model.FavRepoEntity;
import org.itskool.github65apps.domain.model.SearchText;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.Observable;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

/**
 * Created by Vanger on 30.12.2017.
 */

public class PersistRepository {

    private static final String LAST_KEYWORD = "LAST_KEYWORD";
    private static final String DEFAULT_KEYWORD = "android";
    private static final String FAVORITES = "FAVORITES";
    private static final String FAVS_SPLITTER = "/";

    private final SharedPreferences prefs;
    private final ReactiveEntityStore<Persistable> db;

    public PersistRepository(SharedPreferences prefs, ReactiveEntityStore<Persistable> db) {
        this.prefs = prefs;
        this.db = db;
    }

    public String getLastKeyword() {
        return prefs.getString(LAST_KEYWORD, DEFAULT_KEYWORD);
    }

    public void setLastKeyword(String keyword) {
        prefs.edit().putString(LAST_KEYWORD, keyword).apply();
    }


    public List<Integer> getFavorites(){
        return db.select(FavRepoEntity.class)
                .get()
                .observable()
                .map(FavRepoEntity::favoriteId)
                .toList().blockingGet();
    }

    public void changeFavorites(int gitId){
        int count = db.count(FavRepoEntity.class).where(FavRepoEntity.FAVORITE_ID.eq(gitId)).get().value();
        if (count==1){
            db.delete(FavRepoEntity.class).where(FavRepoEntity.FAVORITE_ID.eq(gitId)).get().value();
        } else {
            FavRepoEntity favRepo = new FavRepoEntity();
            favRepo.favoriteId(gitId);
            db.insert(favRepo).subscribe();
        }
    }

    public List<Integer> getFavoritesPrefs() {
        List<Integer> favsInt = new ArrayList<>();
        String favs = prefs.getString(FAVORITES, null);
        if (favs != null) {
            favsInt = Observable.fromArray(favs.split(FAVS_SPLITTER))
                    .map(Integer::valueOf)
                    .toList().blockingGet();
        }
        return favsInt;
    }

    public void saveFavoritesPrefs(List<Integer> favoriteIds) {
        StringBuilder favsString = new StringBuilder();
        Iterator<Integer> i = favoriteIds.iterator();
        while (i.hasNext()) {
            favsString.append(i.next());
            favsString.append(FAVS_SPLITTER);
        }
        Log.d(App.TAG, "PersistManager::saveFavoritesPrefs: " + favsString.toString());

        prefs.edit().putString(FAVORITES, favsString.substring(0, favsString.length() - 1)).apply();
    }

    public Observable<List<SearchText>> keywords(String in) {
        Log.d(App.TAG, "keywords count: " + db.count(SearchText.class).get().value());
        return db.select(SearchText.class)
//                .where(SearchTextEntity.QUERY.like("%" + in + "%"))
                .get().observableResult()
                .map(l -> l.toList());
/*                .flatMap(Observable::fromIterable)
                .map(SearchText::query)
                .concatMap(s -> Observable.fromArray(s.split(" ")))
                .distinct()
                .subscribeOn(Schedulers.io());*/
    }
}
