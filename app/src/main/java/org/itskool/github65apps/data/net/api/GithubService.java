package org.itskool.github65apps.data.net.api;

import org.itskool.github65apps.data.net.SearchDTO;
import org.itskool.github65apps.data.net.UserDTO;

import io.reactivex.Observable;
import retrofit2.Response;


/**
 * Created by Vanger on 07.11.2017.
 */

public class GithubService {
    private GithubApi mGithubApi;

    public GithubService(GithubApi githubApi) {
        mGithubApi = githubApi;
    }

    public Observable<UserDTO> signIn(String token) {
        return mGithubApi.signIn(token);
    }

    public Observable<Response<SearchDTO>> search(String text) {
        return mGithubApi.search(text);
    }

    public Observable<SearchDTO> search(String text, int page) {
        return mGithubApi.search(text, page);
    }

}
