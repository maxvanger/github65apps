package org.itskool.github65apps.data.db;

import android.support.annotation.NonNull;

/**
 * Created by Vanger on 13.11.2017.
 */

public class ReposTable {
    public static final String TABLE_NAME = "repositories";

    public static final String ID = "_id";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String CREATE_AT = "create_at";
    public static final String UPDATE_AT = "update_at";
    public static final String HTML_URL = "html_url";
    public static final String STARS = "stars";

    public static final String USER_NAME = "user_name";
    public static final String USER_AVA = "user_ava";

    private ReposTable() {
        throw new IllegalStateException("No instances please");
    }

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE " + TABLE_NAME + "("
                + ID + " INTEGER NOT NULL PRIMARY KEY, "
                + TITLE + "TEXT, "
                + DESCRIPTION + "TEXT, "
                + CREATE_AT + "TEXT, "
                + UPDATE_AT + "TEXT, "
                + HTML_URL + "TEXT, "
                + STARS + "INTEGER, "
                + USER_NAME + "TEXT, "
                + USER_AVA + "TEXT, "
                + ");";
    }
}
