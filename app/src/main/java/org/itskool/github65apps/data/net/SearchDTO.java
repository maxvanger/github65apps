package org.itskool.github65apps.data.net;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Vanger on 02.11.2017.
 */

public class SearchDTO {
    @SerializedName("total_count")
    private int count;
    @SerializedName("incomplete_results")
    private boolean isIncomplete;
    @SerializedName("items")
    private List<RepoDTO> items;

    public int getCount() {
        return count;
    }

    public boolean isIncomplete() {
        return isIncomplete;
    }

    public List<RepoDTO> getItems() {
        return items;
    }
}
