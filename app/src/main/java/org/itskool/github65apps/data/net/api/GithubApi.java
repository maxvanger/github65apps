package org.itskool.github65apps.data.net.api;

import org.itskool.github65apps.data.net.SearchDTO;
import org.itskool.github65apps.data.net.UserDTO;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

/**
 * Created by Vanger on 06.11.2017.
 */

public interface GithubApi {

    @GET("/user")
    Observable<UserDTO> signIn(@Header("Authorization") String token);

    @GET("/search/repositories?sort=stars")
    Observable<Response<SearchDTO>> search(@Query("q") String query);

    @GET("/search/repositories?sort=stars")
    Observable<SearchDTO> search(@Query("q") String query, @Query("page") int page);
}
