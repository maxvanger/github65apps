package org.itskool.github65apps.data.db;

import android.util.Log;

import org.itskool.github65apps.App;
import org.itskool.github65apps.data.core.LocalSource;
import org.itskool.github65apps.data.core.PageNotFoundException;
import org.itskool.github65apps.domain.model.Repo;
import org.itskool.github65apps.domain.model.RepoType;
import org.itskool.github65apps.domain.model.SearchText;
import org.itskool.github65apps.domain.model.SearchTextEntity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.requery.BlockingEntityStore;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveResult;

import static org.itskool.github65apps.App.PAGE_SIZE;

/**
 * Created by Vanger on 03.12.2017.
 */

public class RequerySource implements LocalSource {

    private final ReactiveEntityStore<Persistable> data;
    private int pagesCount;

    @Inject
    public RequerySource(ReactiveEntityStore<Persistable> data) {
        this.data = data;
    }

    @Override
    public Single<List<Repo>> loadPage(String keywords, int page) {
        Log.d(App.TAG, "requery page: " + page);

        if (!hasMorePage(page)) return Single.error(new PageNotFoundException());

        String query = "%" + keywords + "%";
        return data.select(Repo.class)
                .where(RepoType.DESCRIPTION.like(query)
                        .or(RepoType.TITLE.like(query)))
                .orderBy(RepoType.STARS.desc())
                .limit(PAGE_SIZE).offset(PAGE_SIZE * page)
                .get().observable()
                .toList();
//                .observableResult()
//                .map(repos -> repos.toList());
    }

    @Override
    public void saveAll(List<Repo> list, String text) {
        saveKeyword(text);

        BlockingEntityStore<Persistable> dbStore = data.toBlocking();
        dbStore.runInTransaction(() -> {
            int c;
            for (Repo repo : list) {
                c = dbStore.count(Repo.class).where(RepoType.TITLE.eq(repo.title())).get().value();
                if (c > 0) {
                    dbStore.update(repo);
                } else {
                    dbStore.insert(repo);
                }
            }
            return null;
        });
    }

    @Override
    public void save(Repo repo, String text) {
        saveKeyword(text);
        int c = data.count(Repo.class)
                .where(RepoType.TITLE.eq(repo.title()))
                .get().value();
        if (c > 0) {
            data.update(repo).toObservable().subscribe();
        } else {
            data.insert(repo).toObservable().subscribe();
        }
    }

    private void saveKeyword(String keyword) {
        int c = data.count(SearchText.class)
                .where(SearchTextEntity.QUERY.eq(keyword))
                .get().value();
        if (c == 0) {
            SearchTextEntity search = new SearchTextEntity();
            search.query(keyword);
            data.insert(search).subscribe();
        }
    }

    public boolean hasMorePage(int page) {
        pagesCount = (getItemsCount() + PAGE_SIZE - 1) / PAGE_SIZE;
        return page <= pagesCount;
    }

    @Override
    public int getItemsCount() {
        return data.count(Repo.class).get().value();
    }

    public ReactiveResult<Repo> requeryResult(String query) {
        return data.select(Repo.class)
                .where(RepoType.DESCRIPTION.like(query).or(RepoType.TITLE.like(query)))
                .orderBy(RepoType.STARS.desc())
                .get();
    }

}
