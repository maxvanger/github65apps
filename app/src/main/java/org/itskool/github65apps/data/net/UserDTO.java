package org.itskool.github65apps.data.net;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vanger on 07.11.2017.
 */

public class UserDTO {
    @SerializedName("login")
    private String login;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public String login() {
        return login;
    }

    public String avatarUrl() {
        return avatarUrl;
    }
}
