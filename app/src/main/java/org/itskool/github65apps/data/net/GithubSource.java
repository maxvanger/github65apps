package org.itskool.github65apps.data.net;

import android.util.Log;

import org.itskool.github65apps.App;
import org.itskool.github65apps.data.core.NetSource;
import org.itskool.github65apps.data.core.PageNotFoundException;
import org.itskool.github65apps.data.net.api.GithubService;
import org.itskool.github65apps.domain.model.Repo;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by Vanger on 03.12.2017.
 */

public class GithubSource implements NetSource {

    GithubService github;
    private int pagesCount;
    private int itemsCount;

    @Inject
    public GithubSource(GithubService service) {
        github = service;
    }

    @Override
    public Single<List<Repo>> loadPage(String text, int page) {
        Log.d(App.TAG, "net page: " + page);
        Single<List<Repo>> result;

        if (page == 1) {
            pagesCount = 0;
            result = github.search(text)
                    .flatMap(response -> {
                        okhttp3.Headers headers = response.headers();
                        String navUrls = headers.get("Link");
                        if (navUrls == null)
                            throw new IllegalStateException("Can't find Link header at api.github.com response");
                        String lastPage = navUrls.substring(navUrls.lastIndexOf("page=") + 5, navUrls.lastIndexOf(">"));
                        pagesCount = Integer.valueOf(lastPage);
                        itemsCount = response.body().getCount();
                        Log.d(App.TAG, "Last page: " + lastPage);
                        return Observable.fromIterable(response.body().getItems());
                    })
                    .map(Repo::create)
                    .toList();
        } else {
            if (page > pagesCount) return Single.error(new PageNotFoundException());
            result = github.search(text, page)
                    .flatMap(response -> Observable.fromIterable(response.getItems()))
                    .map(Repo::create)
                    .toList();
//                .concatWith(defer(()->loadPage(text, page+1)));
        }
        return result;
    }

    public boolean isLastPage(int page) {
        return page >= pagesCount;
    }

    @Override
    public void save(Object o, String tag) {
        throw new UnsupportedOperationException("Saving to network source not implemented");
    }

    @Override
    public void saveAll(List list, String tag) {
        throw new UnsupportedOperationException("Saving to network source not implemented");
    }

    @Override
    public int getItemsCount() {
        return itemsCount;
    }
}
