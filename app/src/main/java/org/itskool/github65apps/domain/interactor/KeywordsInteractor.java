package org.itskool.github65apps.domain.interactor;

import org.itskool.github65apps.data.core.LocalSource;
import org.itskool.github65apps.data.core.PersistRepository;
import org.itskool.github65apps.domain.model.SearchText;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Vanger on 30.12.2017.
 */

public class KeywordsInteractor {
    PersistRepository persist;

    public KeywordsInteractor(PersistRepository persist) {

        this.persist = persist;
    }

    public String lastKeyword() {
        return persist.getLastKeyword();
    }

    public void lastKeyword(String keyword) {
        persist.setLastKeyword(keyword);
    }

    public Observable<List<SearchText>> load(String in) {
        return persist.keywords(in);
    }
}
