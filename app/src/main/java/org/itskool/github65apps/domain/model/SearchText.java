package org.itskool.github65apps.domain.model;

import android.os.Parcelable;

import java.util.List;

import io.requery.Convert;
import io.requery.Entity;
import io.requery.Generated;
import io.requery.Key;
import io.requery.Persistable;

/**
 * Created by Vanger on 26.11.2017.
 */
@Entity
public interface SearchText extends Persistable {
/*    @Key
    @Generated
    Long id();*/

    String query();

    void query(String text);
/*
    @Convert(ListLongConverter.class)
    List<Long> repoIds();

    @Convert(ListLongConverter.class)
    void repoIds(List<Long> id);*/
}
