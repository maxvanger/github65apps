package org.itskool.github65apps.domain.interactor;

import android.util.Log;

import org.itskool.github65apps.App;
import org.itskool.github65apps.data.core.GithubRepository;
import org.itskool.github65apps.domain.model.Repo;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.subjects.ReplaySubject;
import io.reactivex.subjects.Subject;
import io.requery.reactivex.ReactiveResult;

/**
 * Created by Vanger on 03.11.2017.
 */
public class RepoListInteractor {

    final private GithubRepository gitRepository;

    @Inject
    public RepoListInteractor(GithubRepository githubRepository) {
        this.gitRepository = githubRepository;
    }

    public Observable<List<Repo>> loadData(String search) {
        Log.d(App.TAG, "RepoListInteractor.loadData() " + search);
        return gitRepository.search(search);
    }

    public void nextPage() {
        Log.d(App.TAG, "RepoListInteractor.nextPage()");
        gitRepository.nextPage();
    }


    public void prevPageRequest(Observable<Boolean> position) {
/*        position.distinctUntilChanged()
                .filter(b -> b)
                .subscribe(b -> {
                    Log.d(App.TAG, "presenter: PREV page request");
                    git.prevPage()
                            .subscribe(list -> repos
                                    .repeat(1)
                                    .subscribe(
                                            this::onLoadSuccess,
                                            this::onLoadFail));
                });*/
    }

    public ReactiveResult<Repo> getResults(String query) {
        return gitRepository.requeryData(query);
    }
}
