package org.itskool.github65apps.domain.interactor;

import android.util.Log;

import org.itskool.github65apps.App;
import org.itskool.github65apps.data.core.DataSource;
import org.itskool.github65apps.data.core.LocalSource;
import org.itskool.github65apps.data.core.NetSource;
import org.itskool.github65apps.domain.model.Repo;
import org.itskool.github65apps.domain.model.SearchText;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import io.requery.reactivex.ReactiveResult;

/**
 * Created by Vanger on 03.12.2017.
 */

public class GitSearchInteractor {

    private final DataSource<Repo> local;
    private final DataSource<Repo> net;

    private boolean isNetAvailable;
    private int currentPage;
    private String query;

    private Subject<List<Repo>> repoList;

    @Inject
    public GitSearchInteractor(LocalSource local, NetSource net) {
        this.local = local;
        this.net = net;
    }

    public Observable<List<Repo>> search(String text) {
        query = text;
        currentPage = 1;
        repoList = PublishSubject.create();

        loadPage();
        return repoList;
    }

    private void loadPage() {
        Log.d(App.TAG, "page: " + currentPage);

        isNetAvailable = App.APP.isNetworkAvailable();

        if (isNetAvailable) {
            net.loadPage(query, currentPage)
                    .subscribeOn(Schedulers.io())
                    .doOnSuccess(list -> {
                        local.saveAll(list, query);
                        repoList.onNext(list);
                    })
                    .doOnError(e -> repoList.onComplete())
                    .subscribe(l -> {
                    }, e -> Log.d(App.TAG, "page not found:" + e));

        } else {
            local.loadPage(query, currentPage)
                    .subscribeOn(Schedulers.io())
                    .doOnSuccess(repoList::onNext)
                    .subscribe(l -> {
                    }, e -> Log.d(App.TAG, e.getMessage()));
        }
    }


    public void nextPage() {
        currentPage++;
        loadPage();
//        return loadPage();
/*                .concatWith(defer(() -> nextPage()))
                .scan((total, chunk) -> {
                    Log.d(App.TAG, "total: " + total.size() + "// chunk: " + chunk.size());
                        List<Repo> list = new ArrayList<>(total);
                        list.addAll(chunk);
                        return list;
                })*/
    }

    public ReactiveResult<Repo> requeryData(String query) {
        return ((LocalSource) local).requeryResult(query);
    }

}
