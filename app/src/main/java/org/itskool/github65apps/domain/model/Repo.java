package org.itskool.github65apps.domain.model;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

import org.itskool.github65apps.data.net.RepoDTO;

import io.requery.Entity;
import io.requery.Key;
import io.requery.Persistable;

/**
 * Created by Vanger on 12.11.2017.
 */
@Entity
@AutoValue
public abstract class Repo implements Parcelable, Persistable {

    static Builder builder() {
        return new AutoValue_Repo.Builder();
    }

    public static Repo create(RepoDTO dto) {
        return new AutoValue_Repo(
                dto.gitId(),
                dto.fullName(),
                dto.description(),
                dto.createdAt(),
                dto.updateAt(),
                dto.starsCount(),
                dto.htmlUrl(),
                dto.owner().login(),
                dto.owner().avatarUrl(),
                dto.watchersCount(),
                dto.forksCount()
        );
    }

    @Key
    public abstract int gitId();

    public abstract String title();

    @Nullable
    public abstract String description();

    @Nullable
    public abstract String createdAt();

    @Nullable
    public abstract String updateAt();

    public abstract int stars();

    public abstract String htmlUrl();

    public abstract String userName();

    @Nullable
    public abstract String userAva();

    public abstract int watchers();

    public abstract int forks();

    @AutoValue.Builder
    static abstract class Builder {
        abstract Builder title(String title);

        abstract Builder description(String descr);

        abstract Builder createdAt(String createAt);

        abstract Builder updateAt(String update);

        abstract Builder stars(int count);

        abstract Builder htmlUrl(String url);

        abstract Builder userName(String name);

        abstract Builder userAva(String avaUrl);

        abstract Builder gitId(int id);

        abstract Builder watchers(int count);

        abstract Builder forks(int count);

        abstract Repo build();
    }
}
