package org.itskool.github65apps.domain.model;

/**
 * Created by Vanger on 12.11.2017.
 */
public class Owner {
    private String name;
    private String avaUrl;

    public Owner(String name, String avaUrl) {
        this.name = name;
        this.avaUrl = avaUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvaUrl() {
        return avaUrl;
    }

    public void setAvaUrl(String avaUrl) {
        this.avaUrl = avaUrl;
    }
}
