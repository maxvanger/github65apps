package org.itskool.github65apps.domain.interactor;

import org.itskool.github65apps.data.core.PersistRepository;

import java.util.List;

/**
 * Created by Vanger on 06.01.2018.
 */

public class FavoritesInteractor {
    private List<Integer> favoriteIds;

    final private PersistRepository pm;

    public FavoritesInteractor(PersistRepository pm) {
        this.pm = pm;
    }

    public List<Integer> getFavoriteIds() {
        return pm.getFavorites();
/*
        if (favoriteIds == null) {
            favoriteIds = pm.getFavoritesPrefs();
        }
        return favoriteIds;
*/
    }

    public void changeFavorite(int gitId){
        pm.changeFavorites(gitId);
    }


    //with saving in SharedPreferences
    public void addFavorite(Integer favoriteId) {
        favoriteIds.add(favoriteId);
        commitFavorite();
    }

    public void removeFavorite(Integer favId) {
        favoriteIds.remove(favId);
        commitFavorite();
    }

    public void commitFavorite(){
        pm.saveFavoritesPrefs(favoriteIds);
    }
}
