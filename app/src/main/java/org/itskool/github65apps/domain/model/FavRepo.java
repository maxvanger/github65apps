package org.itskool.github65apps.domain.model;

import io.requery.Entity;

/**
 * Created by Vanger on 06.01.2018.
 */
@Entity
public interface FavRepo {
    int favoriteId();
}
