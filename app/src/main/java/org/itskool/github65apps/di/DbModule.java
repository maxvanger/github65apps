package org.itskool.github65apps.di;

import android.content.Context;

import org.itskool.github65apps.BuildConfig;
import org.itskool.github65apps.data.core.LocalSource;
import org.itskool.github65apps.data.db.RequerySource;
import org.itskool.github65apps.domain.model.Models;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.android.sqlite.DatabaseSource;
import io.requery.reactivex.ReactiveEntityStore;
import io.requery.reactivex.ReactiveSupport;
import io.requery.sql.Configuration;
import io.requery.sql.ConfigurationBuilder;
import io.requery.sql.EntityDataStore;
import io.requery.sql.TableCreationMode;

/**
 * Created by Vanger on 12.11.2017.
 */
@Module
public class DbModule {

    private LocalSource requery;

    @Provides
    @Singleton
    public LocalSource provideRequery(ReactiveEntityStore<Persistable> store) {
        if (requery == null) {
            requery = new RequerySource(store);
        }
        return requery;
    }

    @Provides
    @Singleton
    ReactiveEntityStore<Persistable> provideStore(Configuration config) {
        return ReactiveSupport.toReactiveStore(new EntityDataStore<Persistable>(config));
    }

    @Provides
    @Singleton
    Configuration provideConfiguration(DatabaseSource source) {
        return new ConfigurationBuilder(source, Models.DEFAULT)
                .useDefaultLogging()
                .setWriteExecutor(Executors.newSingleThreadExecutor())
                .build();
    }

    @Provides
    @Singleton
    DatabaseSource provideDatabaseSource(Context context) {
        final DatabaseSource source = new DatabaseSource(context, Models.DEFAULT, 1);
        if (BuildConfig.DEBUG) {
            source.setTableCreationMode(TableCreationMode.DROP_CREATE);
        }
        return source;
    }
}
