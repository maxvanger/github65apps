package org.itskool.github65apps.di;

import org.itskool.github65apps.di.scope.RepoList;
import org.itskool.github65apps.presentation.repolist.ListFragment;
import org.itskool.github65apps.presentation.repolist.PagerFragment;
import org.itskool.github65apps.presentation.repolist.RepoListPresenter;

import dagger.Subcomponent;

/**
 * Created by Vanger on 12.11.2017.
 */
@Subcomponent(modules = {RepoListModule.class})
@RepoList
public interface RepoListComponent {
    RepoListPresenter getListPresenter();
    void inject(ListFragment fragment);
    void inject(PagerFragment fragment);
}
