package org.itskool.github65apps.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Vanger on 04.12.2017.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface GithubSearch {
}
