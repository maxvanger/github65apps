package org.itskool.github65apps.di;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.itskool.github65apps.data.core.GithubRepository;
import org.itskool.github65apps.data.core.PersistRepository;
import org.itskool.github65apps.domain.interactor.FavoritesInteractor;
import org.itskool.github65apps.domain.interactor.KeywordsInteractor;
import org.itskool.github65apps.domain.interactor.RepoListInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.requery.Persistable;
import io.requery.reactivex.ReactiveEntityStore;

/**
 * Created by Vanger on 07.11.2017.
 */
@Module
public class AppModule {
    private Context context;

    public AppModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    public KeywordsInteractor provideKeywordsInteractor(PersistRepository pm) {
        return new KeywordsInteractor(pm);
    }

    @Provides
    @Singleton
    public FavoritesInteractor provideFavoritesInteractor(PersistRepository pm) {
        return new FavoritesInteractor(pm);
    }

    @Provides
    @Singleton
    public RepoListInteractor provideRepoListInteractor(GithubRepository repository) {
        return new RepoListInteractor(repository);
    }

    @Provides
    @Singleton
    public PersistRepository providePersistRepository(SharedPreferences prefs, ReactiveEntityStore<Persistable> db) {
        return new PersistRepository(prefs, db);
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

}


