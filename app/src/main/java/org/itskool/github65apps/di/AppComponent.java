package org.itskool.github65apps.di;

import android.content.Context;

import com.arellomobile.mvp.MvpAppCompatFragment;

import org.itskool.github65apps.data.core.LocalSource;
import org.itskool.github65apps.data.core.NetSource;
import org.itskool.github65apps.presentation.detail.DetailFragment;
import org.itskool.github65apps.presentation.detail.DetailModule;
import org.itskool.github65apps.presentation.main.MainActivity;
import org.itskool.github65apps.presentation.main.MainActivityModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Vanger on 05.11.2017.
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, DbModule.class})
public interface AppComponent {
/*    Context getContext();

    NetSource getNetSource();

    LocalSource getLocalSource();*/

    RepoListComponent repoListComponent(RepoListModule module);

    DetailFragment.DetailComponent detailComponent(DetailModule module);

    MainActivity.Component mainActivity(MainActivityModule module);
}
