package org.itskool.github65apps.di;

import org.itskool.github65apps.data.core.NetSource;
import org.itskool.github65apps.data.net.GithubSource;
import org.itskool.github65apps.data.net.api.GithubApi;
import org.itskool.github65apps.data.net.api.GithubService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Vanger on 10.11.2017.
 */
@Module(includes = {RetrofitModule.class})
public class NetworkModule {

    private NetSource netSource;

    @Provides
    @Singleton
    public NetSource provideGithubSource(GithubService service) {
        if (netSource == null) {
            netSource = new GithubSource(service);
        }
        return netSource;
    }

    @Provides
    @Singleton
    public GithubService provideGithubService(GithubApi api) {
        return new GithubService(api);
    }

    @Provides
    @Singleton
    public GithubApi provideGithubApi(Retrofit retrofit) {
        return retrofit.create(GithubApi.class);
    }
}
