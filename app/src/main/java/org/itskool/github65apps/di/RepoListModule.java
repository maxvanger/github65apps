package org.itskool.github65apps.di;

import org.itskool.github65apps.di.scope.RepoList;
import org.itskool.github65apps.domain.interactor.KeywordsInteractor;
import org.itskool.github65apps.domain.interactor.RepoListInteractor;
import org.itskool.github65apps.presentation.repolist.RepoAdapter;
import org.itskool.github65apps.presentation.repolist.RepoListPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Vanger on 12.11.2017.
 */
@Module
public class RepoListModule {

    @Provides
    @RepoList
    public RepoAdapter provideRepoAdapter() {
        return new RepoAdapter();
    }

    @Provides
    @RepoList
    public RepoListPresenter provideListPresenter(RepoListInteractor repos, KeywordsInteractor keywords) {
        return new RepoListPresenter(repos, keywords);
    }

}
