package org.itskool.github65apps;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.squareup.leakcanary.LeakCanary;

import org.itskool.github65apps.di.AppComponent;
import org.itskool.github65apps.di.AppModule;
import org.itskool.github65apps.di.DaggerAppComponent;
import org.itskool.github65apps.di.RepoListComponent;
import org.itskool.github65apps.di.RepoListModule;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * Created by Vanger on 07.11.2017.
 */

public class App extends Application {

    public static final String TAG = "GitApp";
    public static final int PAGE_SIZE = 30;

    private static RepoListComponent listComponent;
    public static App APP;
    private AppComponent appComponent;

    private ConnectivityManager cm;
    private Cicerone<Router> cicerone;

    public AppComponent appComponent() {
        return appComponent;
    }

    public static RepoListComponent repoListComponent() {
        if (listComponent == null) {
            listComponent = APP.appComponent().repoListComponent(new RepoListModule());
        }
        return listComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        APP = this;

        cicerone = Cicerone.create();

        if (LeakCanary.isInAnalyzerProcess(this)) return;
        LeakCanary.install(this);

        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public boolean isNetworkAvailable() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public NavigatorHolder navigatorHolder(){
        return cicerone.getNavigatorHolder();
    }

    public Router router(){
        return cicerone.getRouter();
    }

    public void freeComponents() {
        appComponent = null;
        listComponent = null;
    }
}
